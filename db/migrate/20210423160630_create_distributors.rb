class CreateDistributors < ActiveRecord::Migration[6.1]
  def change
    create_table :distributors do |t|
      t.string :code
      t.string :name
      t.string :address
      t.string :contact_person
      t.string :contact_number

      t.timestamps
    end
  end
end
