class AddQuantityToOrderProductMapping < ActiveRecord::Migration[6.1]
  def change
    add_column :order_product_mappings, :quantity, :integer
  end
end
