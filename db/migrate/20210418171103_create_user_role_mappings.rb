class CreateUserRoleMappings < ActiveRecord::Migration[6.1]
  def change
    create_table :user_role_mappings do |t|
      t.references :user, null: false, foreign_key: true
      t.references :role, null: false, foreign_key: true

      t.timestamps
    end
  end
end
