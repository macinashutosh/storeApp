require "test_helper"

class InventoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inventory = inventories(:one)
  end

  test "should get index" do
    get inventories_url
    assert_response :success
  end

  test "should get new" do
    get new_inventory_url
    assert_response :success
  end

  test "should create inventory" do
    assert_difference('Inventory.count') do
      post inventories_url, params: { inventory: { delivered_on: @inventory.delivered_on, distributor_id: @inventory.distributor_id, manufacturer_id: @inventory.manufacturer_id, price: @inventory.price, product_id: @inventory.product_id, units: @inventory.units } }
    end

    assert_redirected_to inventory_url(Inventory.last)
  end

  test "should show inventory" do
    get inventory_url(@inventory)
    assert_response :success
  end

  test "should get edit" do
    get edit_inventory_url(@inventory)
    assert_response :success
  end

  test "should update inventory" do
    patch inventory_url(@inventory), params: { inventory: { delivered_on: @inventory.delivered_on, distributor_id: @inventory.distributor_id, manufacturer_id: @inventory.manufacturer_id, price: @inventory.price, product_id: @inventory.product_id, units: @inventory.units } }
    assert_redirected_to inventory_url(@inventory)
  end

  test "should destroy inventory" do
    assert_difference('Inventory.count', -1) do
      delete inventory_url(@inventory)
    end

    assert_redirected_to inventories_url
  end
end
