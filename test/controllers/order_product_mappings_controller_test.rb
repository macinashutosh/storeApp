require "test_helper"

class OrderProductMappingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order_product_mapping = order_product_mappings(:one)
  end

  test "should get index" do
    get order_product_mappings_url
    assert_response :success
  end

  test "should get new" do
    get new_order_product_mapping_url
    assert_response :success
  end

  test "should create order_product_mapping" do
    assert_difference('OrderProductMapping.count') do
      post order_product_mappings_url, params: { order_product_mapping: { order_id: @order_product_mapping.order_id, product_id: @order_product_mapping.product_id } }
    end

    assert_redirected_to order_product_mapping_url(OrderProductMapping.last)
  end

  test "should show order_product_mapping" do
    get order_product_mapping_url(@order_product_mapping)
    assert_response :success
  end

  test "should get edit" do
    get edit_order_product_mapping_url(@order_product_mapping)
    assert_response :success
  end

  test "should update order_product_mapping" do
    patch order_product_mapping_url(@order_product_mapping), params: { order_product_mapping: { order_id: @order_product_mapping.order_id, product_id: @order_product_mapping.product_id } }
    assert_redirected_to order_product_mapping_url(@order_product_mapping)
  end

  test "should destroy order_product_mapping" do
    assert_difference('OrderProductMapping.count', -1) do
      delete order_product_mapping_url(@order_product_mapping)
    end

    assert_redirected_to order_product_mappings_url
  end
end
