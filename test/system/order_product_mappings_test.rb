require "application_system_test_case"

class OrderProductMappingsTest < ApplicationSystemTestCase
  setup do
    @order_product_mapping = order_product_mappings(:one)
  end

  test "visiting the index" do
    visit order_product_mappings_url
    assert_selector "h1", text: "Order Product Mappings"
  end

  test "creating a Order product mapping" do
    visit order_product_mappings_url
    click_on "New Order Product Mapping"

    fill_in "Order", with: @order_product_mapping.order_id
    fill_in "Product", with: @order_product_mapping.product_id
    click_on "Create Order product mapping"

    assert_text "Order product mapping was successfully created"
    click_on "Back"
  end

  test "updating a Order product mapping" do
    visit order_product_mappings_url
    click_on "Edit", match: :first

    fill_in "Order", with: @order_product_mapping.order_id
    fill_in "Product", with: @order_product_mapping.product_id
    click_on "Update Order product mapping"

    assert_text "Order product mapping was successfully updated"
    click_on "Back"
  end

  test "destroying a Order product mapping" do
    visit order_product_mappings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Order product mapping was successfully destroyed"
  end
end
