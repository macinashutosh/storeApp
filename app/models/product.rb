class Product < ApplicationRecord
  belongs_to :manufacturer
  has_many :order_product_mappings
  has_many :orders, through: :order_product_mappings
end
