class Inventory < ApplicationRecord
  belongs_to :product
  belongs_to :distributor
  belongs_to :manufacturer
end
