class Manufacturer < ApplicationRecord
    validates :name, presence: true, uniqueness: true
    validate :checkForNameRegex

    def checkForNameRegex
        if Manufacturer.where('lower(name) like ?', "%#{name.downcase}%").present?
            errors.add(:name, "Name already present")
            return false
        end
    end
end
