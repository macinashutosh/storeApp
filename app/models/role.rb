class Role < ApplicationRecord
    validates :name, presence: true, uniqueness: true 

    has_many :user_role_mappings
    has_many :users, through: :user_role_mappings

    def add_role(user)
        user.add_role(self)
    end
end
