class Order < ApplicationRecord
  belongs_to :customer
  has_many :order_product_mappings
  has_many :products, through: :order_product_mappings
end
