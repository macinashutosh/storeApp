class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :lockable, :trackable

    has_many :user_role_mappings
    has_many :roles, through: :user_role_mappings

    # check whether user has the role
    def has_role?(role)
      roles.any? { |r| r.name.underscore.to_sym == role.to_sym}
    end

    def admin?
      has_role? (:admin)
    end

    def employee?
      has_role? (:employee)
    end

    def add_role(roleObj)
      return if roleObj.nil? || has_role?(roleObj.name)
      
      UserRoleMapping.create(user: self, role: roleObj)
    end
end
