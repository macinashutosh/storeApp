json.extract! order_product_mapping, :id, :order_id, :product_id, :created_at, :updated_at
json.url order_product_mapping_url(order_product_mapping, format: :json)
