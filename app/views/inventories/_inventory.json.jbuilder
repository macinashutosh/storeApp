json.extract! inventory, :id, :product_id, :units, :price, :delivered_on, :distributor_id, :manufacturer_id, :created_at, :updated_at
json.url inventory_url(inventory, format: :json)
