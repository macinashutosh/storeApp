json.extract! product, :id, :manufacturer_id, :name, :mrp, :created_at, :updated_at
json.url product_url(product, format: :json)
