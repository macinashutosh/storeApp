json.extract! distributor, :id, :code, :name, :address, :contact_person, :contact_number, :created_at, :updated_at
json.url distributor_url(distributor, format: :json)
