class InventoriesController < InheritedResources::Base

  private

    def inventory_params
      params.require(:inventory).permit(:product_id, :units, :price, :delivered_on, :distributor_id, :manufacturer_id)
    end

end
