class ProductsController < InheritedResources::Base

  private

    def product_params
      params.require(:product).permit(:manufacturer_id, :name, :mrp)
    end

end
