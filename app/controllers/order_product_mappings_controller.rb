class OrderProductMappingsController < InheritedResources::Base

  private

    def order_product_mapping_params
      params.require(:order_product_mapping).permit(:order_id, :product_id)
    end

end
