class OrdersController < InheritedResources::Base

  private

    def order_params
      params.require(:order).permit(:customer_id, :price)
    end

end
