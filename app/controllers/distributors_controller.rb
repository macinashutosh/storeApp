class DistributorsController < InheritedResources::Base

  private

    def distributor_params
      params.require(:distributor).permit(:code, :name, :address, :contact_person, :contact_number)
    end

end
