Rails.application.routes.draw do
  resources :order_product_mappings
  resources :orders
  resources :inventories
  resources :customers
  resources :distributors
  resources :products
  resources :manufacturers
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
